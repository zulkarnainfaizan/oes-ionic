import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-video-player',
  templateUrl: './video-player.page.html',
  styleUrls: ['./video-player.page.scss'],
})
export class VideoPlayerPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }
//   constructor(private videoPlayer: VideoPlayer, public modalCtrl: ModalController) {
//   }
//   playVideoLocal() {
//     this.videoPlayer.play('file:///android_asset/www/assets/SampleVideo.mp4').then(() => {
//       console.log('video completed');
//     }).catch(err => {
//       console.log(err);
//     });
//   }

//   playVideoHosted() {
//     this.videoPlayer.play('https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_1mb.mp4').then(() => {
//       console.log('video completed');
//     }).catch(err => {
//       console.log(err);
//     });
//   }
}