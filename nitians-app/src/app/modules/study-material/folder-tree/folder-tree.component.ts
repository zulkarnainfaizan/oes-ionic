import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { FileService } from '../../../services/file.service';
import { takeUntil } from 'rxjs/internal/operators/takeUntil';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-folder-tree',
  templateUrl: './folder-tree.component.html',
  styleUrls: ['./folder-tree.component.scss']
})
export class FolderTreeComponent implements OnInit, OnDestroy {
  refId: any;
  isProcessing: boolean = false;
  parentId: any = null;
  data: any[] = [];
  pathStack = [];
  expanded: any = [];
  selectedWorkLocation: any = {};
  rootFolder: any = {
    id: null,
    name: '',
    childFolders: [],
    isVisible: false,
    level: 0
  };
  tableOptions: any = {
    page: 0,
    rows: 10,
    lengthMenu: [10, 25, 50, 100],
    isSearch: false,
    searchKeyword: "",
    IsAscending: false,
    SortCol: "Dated",
  }
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private fileService: FileService,
    private modalCtrl:ModalController) {
    this.fileService.setRefId.subscribe((refId) => {
      this.refId = refId;
      this.setDefaults();
      this.getList(this.rootFolder);
    });
  }

  ngOnInit(): void {
    this.setDefaults();
    this.getList(this.rootFolder);
  }

  setDefaults() {
    this.rootFolder.name = '';
    this.selectedWorkLocation = this.rootFolder;
  }

  getList(folder) {
    if (folder) {
      if (!folder.id) {
        this.pathStack = [];
      }
      folder.isVisible  = true;
      let parent = JSON.parse(JSON.stringify(folder));// To prevent pass by reference and recursion
      delete parent.parent;
      delete parent.childFolders;
      if (this.selectedWorkLocation) {
        if (this.selectedWorkLocation.level >= folder.level) {
          // backtrack
          for (let index = this.pathStack.length - 1; index >= 0; index--) {
            if (folder.parentId == this.pathStack[index]) {
              break;
            }
            else {
              var nodeCollapsed = this.pathStack.pop();
              console.log('Backtracking' + nodeCollapsed)
            }
          }
        }
      }
      this.selectedWorkLocation = parent;
      this.pathStack.push(parent.id);
      if (!folder.childFolders || !folder.childFolders.length) {
        folder.isFetchingChildren = true;
        this.tableOptions.parentId = folder.id || null;
        this.tableOptions.RefId = this.refId;
        this.fileService.getFolderList(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
          (response: any) => {
            let childFolders = response.records;
            if (response && response.length) {
              for (let index = 0; index < response.length; index++) {
                if (childFolders[index]) {
                  childFolders[index].parent = parent;
                  childFolders[index].level = parent.level + 1;
                }
              }
            }
            folder.childFolders = childFolders;
            console.log(folder.childFolders.length);
            folder.isFetchingChildren = false;

          },
          error => {
            folder.isFetchingChildren = false;
            console.log(error);
          });
        this.fileService.updateList.next(this.tableOptions.parentId);
      }
    }
  }

  openEditModal(folderId) {
    this.fileService.openModal.next(folderId);
  }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onCancel() {
    this.modalCtrl.dismiss(null, 'cancel');
  }
}
