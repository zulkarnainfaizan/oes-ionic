import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule} from '@ionic/angular';

import { StudyMaterialPageRoutingModule } from './study-material-routing.module';

import { StudyMaterialPage } from './study-material.page';
import { FolderTreeComponent } from './folder-tree/folder-tree.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudyMaterialPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [StudyMaterialPage, FolderTreeComponent],
  entryComponents:[FolderTreeComponent]
})
export class StudyMaterialPageModule {}
