import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GroupService } from 'src/app/services/group.service';
import { FileService } from 'src/app/services/file.service';
import { SiteService } from 'src/app/services/site.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { FolderTreeComponent } from './folder-tree/folder-tree.component';
import { ModalController } from '@ionic/angular';
// import { GlobalValues } from '../utilities/global-values';

@Component({
  selector: 'app-study-material',
  templateUrl: './study-material.page.html',
  styleUrls: ['./study-material.page.scss'],
})
export class StudyMaterialPage implements OnInit, OnDestroy {
  classData = [];
  subjectData = [];
  form: FormGroup;
  control: FormControl;
  parentId: any = null;
  codeName: any;
  refId: any;
  siteId: any;

  tableOptions: any = {
    page: 0,
    rows: 10,
    lengthMenu: [10, 25, 50, 100],
    isSearch: false,
    searchKeyword: "",
    SortCol: "Name",
    IsAscending: false,
    selectedCols: []
  }
  rows: any = [];
  tempRows = [];
  isProcessing = false;
  pdfTitle: any = '';
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  constructor(private groupService: GroupService,
    private fileService: FileService,
    private siteService: SiteService,
    private alertifyService: AlertService,
    private formBuilder: FormBuilder,
    private modalCtrl:ModalController
    // public bsModalRef: BsModalRef,
    // private modalService: BsModalService, 
    ) {
    this.form = this.formBuilder.group({
      Class: [''],
      Subject: ['']
    });
    this.fileService.updateList.subscribe((parentId) => {
      this.parentId = parentId;
      this.getList();
    });
    this.fileService.openModal.subscribe((folderId) => {
      // this.openAddModal(folderId);
    });
  }

  ngOnInit(): void {
    this.getGroups();
    this.getSites();
  }

  getGroups() {
    this.groupService.index(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      data => {
        this.classData = [];
        this.classData = data.records;
        this.setClassValue();
      },
      err => {
        console.log(err);
      });
  }

  setClassValue() {
    if (this.classData && this.classData.length) {
      let index = this.classData.findIndex(x => x.name === "11th");
      if (index + "" != "undefined" && index + "" != "null") {
        this.form.controls.Class.setValue(this.classData[index].id);
        this.getSubjects(this.classData[index].id, false);
      }
      else {
        this.form.controls.Class.setValue(this.classData[0].id);
      }
    }
  }

  getSubjects(id, replaceValue) {
    if (replaceValue) {
      id = Number(id.split(": ").pop());
    }
    let modal: any = {
      "id": id,
      "sortCol": "id",
      "isAscending": true,
      "page": 0,
      "rows": 0
    }
    this.groupService.getSubjects(modal).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      data => {
        this.subjectData = [];
        this.subjectData = data.records;
        this.setSubjectValue();
      },
      err => {
        console.log(err);
      });
  }

  setSubjectValue() {
    if (this.subjectData && this.subjectData.length) {
      let index = this.subjectData.findIndex(x => x.subjectName === "Physics");
      if (index + "" != "undefined" && index + "" != "null") {
        this.form.controls.Subject.setValue(this.subjectData[index].id);
        this.refId = this.subjectData[index].id;
        this.codeName = this.subjectData[index].subjectName;
      }
      else {
        this.form.controls.Subject.setValue(this.subjectData[0].id);
        this.refId = this.subjectData[index].id;
        this.codeName = this.subjectData[0].subjectName;
      }
      if (this.refId) {
        this.fileService.setRefId.next(this.refId);
      }
      this.getList();
    }
  }

  setSubject(id: any) {
    if (id) {
      id = Number(id.split(": ").pop());
      if (this.subjectData && this.subjectData.length) {
        let index = this.subjectData.findIndex(x => x.id === id);
        if (index + "" != "undefined" && index + "" != "null") {
          this.refId = this.subjectData[index].id;
          this.codeName = this.subjectData[index].subjectName;
          this.fileService.setRefId.next(this.refId);
          this.getList();
        }
      }
    }
  }

  getList() {
    this.isProcessing = true;
    this.rows = [];
    this.tableOptions.ParentId = this.parentId;
    this.tableOptions.RefId = this.refId;
    this.fileService.getFolderList(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      data => {
        if (this.parentId) {
          this.getFileList(this.parentId, data.records);
        }
        else {
          this.isProcessing = false;
          this.rows = data.records;
        }
        // this.isProcessing = false;
      },
      error => {
        this.isProcessing = false;
        console.log(error);
      })
  }

  getFileList(folderId, rows) {
    this.tableOptions.Id = folderId;
    this.fileService.getFileList(this.tableOptions).pipe(takeUntil(this.ngUnsubscribe)).subscribe(
      data => {
        this.rows = rows;
        this.tempRows = data.records;
        if (this.tempRows && this.tempRows.length) {
          for (var i = 0; i < this.tempRows.length; i++) {
            let file = this.tempRows[i];
            if (file) {
              this.rows.push({
                id: file.id,
                name: file.name,
                uniqueName: file.uniqueName,
                description: file.description,
                link: file.link,
                size: file.size,
                mimeType: file.mimeType,
                folderId: file.folderId,
                extension: file.extension,
                createdOn: file.createdOn,
                isFile: true
              })
            }
          }
        }
        this.isProcessing = false;
      },
      error => {
        this.isProcessing = false;
        console.log(error);
      })
  }

  getSites() {
    this.siteService.index(this.tableOptions).subscribe(resp => {
      if (resp && resp.records && resp.records[0]) {
        this.siteId = resp.records[0].id;
      }
    },
      error => {
        console.log(error);
      })
  }

  // openAddModal(id?: any) {
  //   var config = GlobalValues.getModalConfig();
  //   config.initialState = {
  //     id: id || null,
  //     parentId: this.parentId,
  //     codeName: this.codeName,
  //     siteId: this.siteId,
  //     refId: this.form.get('Subject').value,
  //   }
  //   this.bsModalRef = this.modalService.show(StudyMaterialAddEditComponent, config);
  // }

  // uploadVideo() {
  //   if (this.parentId) {
  //     var config = GlobalValues.getModalConfig();
  //     config.initialState = {
  //       codeName: this.codeName,
  //       siteId: this.siteId,
  //       folderId: this.parentId,
  //     }
  //     this.bsModalRef = this.modalService.show(UploadVideoComponent, config);
  //   }
  //   else {
  //     this.alertifyService.info('Please select any folder to upload video.');
  //   }
  // }

  // uploadAttachment() {
  //   if (this.parentId) {
  //     var config = GlobalValues.getModalConfig();
  //     config.initialState = {
  //       codeName: this.codeName,
  //       siteId: this.siteId,
  //       folderId: this.parentId,
  //     }
  //     this.bsModalRef = this.modalService.show(UploadAttachmentComponent, config);
  //   }
  //   else {
  //     this.alertifyService.info('Please select any folder to upload PDF.');
  //   }
  // }

  // downloadAttachment(id: any, name: any, template: TemplateRef<any>) {
  //   this.pdfTitle = name;
  //   this.bsModalRef = this.modalService.show(template, GlobalValues.getModalConfig());
  //   this.alertifyService.showLoader();
  //   this.fileService.downloadAttachment(id).pipe(takeUntil(this.ngUnsubscribe)).subscribe(blob => {
  //     this.getBase64(blob, (result) => {
  //       var pdfData = result;
  //       pdfData = pdfData.split("base64,").pop();
  //       pdfData = atob(pdfData);
  //       console.log(pdfData)
  //       this.alertifyService.hidePopup();
  //       this.view(pdfData);
  //     },
  //       function (error) {
  //         console.log('Error: ', error);
  //       }
  //     );
  //   },
  //     error => {
  //       this.alertifyService.hidePopup();
  //       this.alertifyService.error(error.message)
  //     });
  // }

  getBase64(file, success, error) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onerror = error;
    reader.onload = () => {
      success(reader.result);
    };
  }

  // view(response) {
  //   // The workerSrc property shall be specified.
  //   pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
  //   var loadingTask = pdfjsLib.getDocument({ data: response });
  //   loadingTask.promise.then(function (pdf) {
  //     console.log('PDF loaded');

  //     // Fetch the first page
  //     var pageNumber = 1;
  //     pdf.getPage(pageNumber).then(function (page) {
  //       console.log('Page loaded');

  //       var scale = 1.5;
  //       var viewport = page.getViewport({ scale: scale });

  //       // Prepare canvas using PDF page dimensions
  //       var canvas = document.getElementById('the-canvas');
  //       var context = canvas['getContext']('2d');
  //       canvas['height'] = viewport.height;
  //       canvas['width'] = viewport.width;

  //       // Render PDF page into canvas context
  //       var renderContext = {
  //         canvasContext: context,
  //         viewport: viewport
  //       };
  //       var renderTask = page.render(renderContext);
  //       renderTask.promise.then(function () {
  //         console.log('Page rendered');
  //       });
  //     });
  //   }, function (reason) {
  //     // PDF loading error
  //     console.error(reason);
  //   });
  // }

  public ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
  viewModal(issueDetails?: any) {
    // const issue = issueDetails;
    // this.activeIssue = issue;
    // const moduleId = (this.globalUtil.getModuleFromControl('Issues') || {}).id;
    this.modalCtrl
      .create({
        component: FolderTreeComponent,
        componentProps: {  },
      })
      .then((modalEl) => {
        modalEl.present();
        return modalEl.onDidDismiss();
      });
  
}

}