// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.page.html',
//   styleUrls: ['./login.page.scss'],
// })
// export class LoginPage implements OnInit {

//   constructor() { }

//   ngOnInit() {
//   }

// }
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {IonSlides } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { AlertService } from 'src/app/services/alert/alert.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';
// import { AuthService } from 'src/app/services/auth-service/auth.service';
// import { AlertService } from 'src/app/services/alert/alert.service';
// import { AuthUtil } from 'src/app/utilities/auth-utilities';
// import { MenuController } from '@ionic/angular';
// import { ModuleService } from 'src/app/services/module/module.service';
// import { GlobalUtil } from 'src/app/utilities/global-utilities';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  isLoading = false;
  tableOptions: any = {
    page: 0,
    rows: 100,
    isSearch: false,
    searchKeyword: '',
    SortCol: 'DisplayOrder',
    IsAscending: true,
    selectedCols: []
};
////////////////////////////////////////////////////////////////////////  
@ViewChild('slideWithNav',{static:true}) slideWithNav: IonSlides;
  // @ViewChild('slideWithNav2') slideWithNav2: Slides;
  // @ViewChild('slideWithNav3') slideWithNav3: Slides;
 
  sliderOne: any;
  // sliderTwo: any;
  // sliderThree: any;
  url: string = "study-material";
 
  //Configuration for each Slider
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
////////////////////////////////////////////////////////////////////////
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
    private tokenStorageService: TokenStorageService, 
    // private authUtil: AuthUtil,
    // private menuCtrl: MenuController,
    // private moduleService: ModuleService,
    // private globalUtil: GlobalUtil
  ) {
    ////////////////////////////////////
    this.sliderOne =
      {
        isBeginningSlide: true,
        isEndSlide: false,
        slidesItems: [
          {
            id: 1,
            image: '../../assets/images/1.jpg'
          },
          {
            id: 2,
            image: '../../assets/images/2.jpg'
          },
          {
            id: 3,
            image: '../../assets/images/3.jpg'
          },
          {
            id: 4,
            image: '../../assets/images/4.jpg'
          },
          {
            id: 5,
            image: '../../assets/images/5.jpg'
          },
          {
            id: 6,
            image: '../../assets/images/6.jpg'
          }
        ]
      };

    /////////////////////////////////////
    this.loginForm = fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
//////////////////////////////////
 //Move to Next slide
 slideNext(object, slideView) {
  slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });
}

//Move to previous slide
slidePrev(object, slideView) {
  slideView.slidePrev(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });;
}

//Method called when slide is changed by drag or navigation
SlideDidChange(object, slideView) {
  this.checkIfNavDisabled(object, slideView);
}
//Call methods to check if slide is first or last to enable disbale navigation  
checkIfNavDisabled(object, slideView) {
  this.checkisBeginning(object, slideView);
  this.checkisEnd(object, slideView);
}

checkisBeginning(object, slideView) {
  slideView.isBeginning().then((istrue) => {
    object.isBeginningSlide = istrue;
  });
}
checkisEnd(object, slideView) {
  slideView.isEnd().then((istrue) => {
    object.isEndSlide = istrue;
  });
}
//////////////////////////////////

  ngOnInit() {}
  ionViewWillEnter() {
    // this.menuCtrl.close('Menu');
  }

  login() {
    const value = this.loginForm.value;
    this.isLoading = !this.isLoading;
    // this.alertService.loading();
    this.authService.login(value).subscribe(
      (res: any) => {
        if (res && res.user && res.token) {
          this.isLoading = !this.isLoading;
         // this.alertService.dismiss();
          // this.alertService.basicAlert('Success', 'Login Successfull!');
          // this.authUtil.login(res);
          // this.router.navigateByUrl('/home');
          // this.getModules(() => {
            this.tokenStorageService.setRedirectUrl(this.url);
            this.tokenStorageService.saveToken(res.token);
            this.tokenStorageService.saveUser(res.user);
            this.router.navigate([this.tokenStorageService.getRedirectUrl()]);
            // this.router.navigate(['/study-material']);
        // });

        }
      },
      error => {
        this.isLoading = !this.isLoading;
        const jStr = JSON.stringify(error);
        console.log(jStr)
        const errRes=JSON.parse(jStr)
        if (errRes['status'] === 0) {
        this.alertService.basicAlert('Error','No Internet connection');
        }
        else if (errRes['status'] === 401) {
        this.alertService.basicAlert('Error','Incorrect Username or Password!');
        }
        else {
        // this.alertService.basicAlert('Error',JSON.stringify(error)+ 'errorZ!');
        }
      }
    );
  }
// //   getModules(callback) {
//     this.moduleService.getModuleList(this.tableOptions).subscribe((res: any) => {
//         if (res) {
//             this.globalUtil.setAllModules(res);
//             callback();
//         }
//     }, error => {
//         console.log(error);
//         callback();
//     });
// }
routeHome(){
  this.router.navigate(['/home']);
}
}

