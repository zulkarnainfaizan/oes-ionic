
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {IonSlides } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth-service/auth.service';
import { AlertService } from 'src/app/services/alert/alert.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  loginForm: FormGroup;
  isLoading = false;
  tableOptions: any = {
    page: 0,
    rows: 100,
    isSearch: false,
    searchKeyword: '',
    SortCol: 'DisplayOrder',
    IsAscending: true,
    selectedCols: []
};
@ViewChild('slideWithNav',{static:true}) slideWithNav: IonSlides;
 
  sliderOne: any;
 
 
  //Configuration for Slider
  slideOptsOne = {
    initialSlide: 0,
    slidesPerView: 1,
    autoplay:true
  };
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private alertService: AlertService,
  ) {
    this.sliderOne =
      {
        isBeginningSlide: true,
        isEndSlide: false,
        slidesItems: [
          {
            id: 1,
            image: '../../assets/images/1.jpeg'
          },
          {
            id: 2,
            image: '../../assets/images/2.jpeg'
          },
          {
            id: 3,
            image: '../../assets/images/3.jpeg'
          },
          {
            id: 4,
            image: '../../assets/images/4.jpeg'
          },
          {
            id: 5,
            image: '../../assets/images/5.jpeg'
          },
          {
            id: 6,
            image: '../../assets/images/6.jpeg'
          },
          {
            id: 7,
            image: '../../assets/images/7.jpeg'
          }
        ]
      };

    this.loginForm = fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
 //Move to Next slide
 slideNext(object, slideView) {
  slideView.slideNext(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });
}

//Move to previous slide
slidePrev(object, slideView) {
  slideView.slidePrev(500).then(() => {
    this.checkIfNavDisabled(object, slideView);
  });;
}

//Method called when slide is changed by drag or navigation
SlideDidChange(object, slideView) {
  this.checkIfNavDisabled(object, slideView);
}
//Call methods to check if slide is first or last to enable disbale navigation  
checkIfNavDisabled(object, slideView) {
  this.checkisBeginning(object, slideView);
  this.checkisEnd(object, slideView);
}

checkisBeginning(object, slideView) {
  slideView.isBeginning().then((istrue) => {
    object.isBeginningSlide = istrue;
  });
}
checkisEnd(object, slideView) {
  slideView.isEnd().then((istrue) => {
    object.isEndSlide = istrue;
  });
}

  ngOnInit() {}

  login() {
    const value = this.loginForm.value;
    this.isLoading = !this.isLoading;
    // this.alertService.loading();
    this.authService.login(value).subscribe(
      (res: any) => {
        if (res && res.user && res.token) {
          this.isLoading = !this.isLoading;
            this.router.navigate(['/study-material']);
        // });

        }
      },
      error => {
        this.isLoading = !this.isLoading;
        const jStr = JSON.stringify(error);
        console.log(jStr)
        const errRes=JSON.parse(jStr)
        if (errRes['status'] === 0) {
        this.alertService.basicAlert('Error','No Internet connection');
        }
        else if (errRes['status'] === 401) {
        this.alertService.basicAlert('Error','Incorrect Username or Password!');
        }
        else {
        // this.alertService.basicAlert('Error',JSON.stringify(error)+ 'errorZ!');
        }
      }
    );
  }
routeLogin(){
  this.router.navigate(['/auth']);
}
}

