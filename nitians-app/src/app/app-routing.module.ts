import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './guards/auth-guard.service';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'auth',
    loadChildren: () => import('./modules/auth/login/login.module').then( m => m.LoginPageModule)
  },
  // {
  //   path: 'study-material',
  //   loadChildren: () => import('./modules/repository/study-material/study-material.module').then( m => m.StudyMaterialPageModule)
  // },
  {
    path: 'video-player',
    loadChildren: () => import('./modules/video-player/video-player.module').then( m => m.VideoPlayerPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'study-material',
    loadChildren: () => import('./modules/study-material/study-material.module').then( m => m.StudyMaterialPageModule),
    canLoad: [AuthGuardService]
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
