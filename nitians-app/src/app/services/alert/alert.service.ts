import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private loadingCtrl: LoadingController, private alertCtrl: AlertController) {
  }
  async viewImage(imageURL, originalFileName, size) {
    // Swal.fire({
    //     html: originalFileName + "<br>" + size,
    //     imageUrl: imageURL,
    //     imageAlt: 'Custom image',
    //     confirmButtonText: 'Close',
    //     confirmButtonColor: '#f96565',
    //     allowOutsideClick: false,
    //     allowEscapeKey: false,
    //     animation: false
    // });
    const alert = await this.alertCtrl.create({
      header: originalFileName,
      message:`<img src="${imageURL}" alt="g-maps" style="border-radius: 2px">`,
      subHeader: size,
      
      buttons: ['OK']
      });
    await alert.present();
}

  async loading() {
    const loading = await this.loadingCtrl.create({
      spinner: 'bubbles',
      message: 'Please wait...',
      translucent: true,
      cssClass: 'custom-class custom-loading'
    });
    return await loading.present();
  }
   async dismiss() {
    return await this.loadingCtrl.dismiss();
  }
  async presentLoading() {
    const loading = await this.loadingCtrl.create({
      message: 'loading',
      duration: 200000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();

    console.log('Loading dismissed!');
  }
  async hideLoading() {
    await this.loadingCtrl.dismiss();
  }

  async basicAlert(title: string, message: string, subHeader= '') {
      const alert = await this.alertCtrl.create({
        header: title,
        subHeader,
        message,
        buttons: ['OK']
        });
      await alert.present();
  }

  async confirmAlert(title: any, message: any, handler: () => void) {
      const alert = await this.alertCtrl.create({
          header: title,
          message,
          buttons: [
            {
              text: 'Cancel',
              role: 'cancel',
            },
            {
              text: 'Ok',
              handler
            }
          ]
        });
      await alert.present();
      }
      async deleteAlert(title: any, message: any, handler: { (): void; (): void; (): void; (): void; (): void; }) {
        const alert = await this.alertCtrl.create({
            header: title,
            message,
            buttons: [
              {
                text: 'Cancel',
                role: 'cancel',
              },
              {
                text: 'Delete',
                handler
              }
            ]
          });
        await alert.present();
        }
}
