// import { Injectable, Inject } from '@angular/core';
// import { Router } from '@angular/router';
// import { JwtHelperService } from "@auth0/angular-jwt";
// import { GenericService } from '../generic/generic.service';
// import { Subject } from 'rxjs';

// @Injectable({
//     providedIn: 'root'
// })
// export class AuthService {
//     helper = new JwtHelperService();
//     userData: any;
//     isLoggedIn = false;
//     setGisDefaults = new Subject<any>();
//     closeModal = new Subject<any>();
//     constructor(private genericService: GenericService, private router: Router) { }

//     login(loginModel: any) {
//         return this.genericService.post('api/auth/login', loginModel);
//     }

//     sendResetLink(userModel: any) {
//         return this.genericService.post('api/auth/SendPasswordResetLink', userModel);
//     }

//     resetPassword(resetModel: any) {
//         return this.genericService.post('api/auth/ResetPassword', resetModel);
//     }

//     authorize() {
//         if (this.loggedIn()) {
//             // If already logged-in, then redirect to home page
//             this.router.navigate(['home']);
//         }
//         else {
//             this.router.navigate(['/session/login']);
//         }
//     }

//     loggedIn() {
//         return !this.helper.isTokenExpired(localStorage.getItem("token"));
//     }

//     async calculateTokenValidity() {
//         var token = this.storage.get('token');
//         if (token) {
//             var currentdate: any = new Date();
//             var tokenValidity: any = this.isTokenExpired();
//             var MS_PER_MINUTE = 60000;
//             var myStartDate = new Date(tokenValidity);
//             var CounterStartTime = myStartDate.getTime();
//             var currentTime = currentdate.getTime();
//             var counter = (CounterStartTime - currentTime) / MS_PER_MINUTE;
//             return counter;
//         } else {
//             return 0;
//         }
//     }

//     async isTokenExpired() {
//         var token = await this.storage.get("token");
//         if (token) {
//             return this.helper.getTokenExpirationDate(token);
//         }
//     }

//     updateSession(Id) {
//         return this.genericService.post('api/auth/UpdateToken', Id);
//     }

//     async getLocalStorageUser() {

//         this.userData = JSON.parse(await this.storage.get("user"));
//         var token = await this.storage.get("token");
//         if (this.userData && token) {
//             this.isLoggedIn = true;
//             return true;
//         } else {
//             this.isLoggedIn = false;
//             return false;
//         }
//     }
// }
import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { JwtHelperService } from "@auth0/angular-jwt";
import { GenericService } from '../generic/generic.service';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    helper = new JwtHelperService();
    userData: any;
    isLoggedIn = false;
    setGisDefaults = new Subject<any>();
    closeModal = new Subject<any>();
    constructor(private genericService: GenericService, private router: Router) { }

    login(loginModel: any) {
        return this.genericService.post('/auth/login', loginModel);
    }

    sendResetLink(userModel: any) {
        return this.genericService.post('api/auth/SendPasswordResetLink', userModel);
    }

    resetPassword(resetModel: any) {
        return this.genericService.post('api/auth/ResetPassword', resetModel);
    }

    authorize() {
        if (this.loggedIn()) {
            // If already logged-in, then redirect to home page
            this.router.navigate(['home']);
        }
        else {
            this.router.navigate(['auth/login']);
        }
    }

    loggedIn() {
        return !this.helper.isTokenExpired(localStorage.getItem("token"));
    }

    calculateTokenValidity() {
        var token = localStorage.getItem('token');
        if (token) {
            var currentdate: any = new Date();
            var tokenValidity: any = this.isTokenExpired();
            var MS_PER_MINUTE = 60000;
            var myStartDate = new Date(tokenValidity);
            var CounterStartTime = myStartDate.getTime();
            var currentTime = currentdate.getTime();
            var counter = (CounterStartTime - currentTime) / MS_PER_MINUTE;
            return counter;
        } else {
            return 0;
        }
    }

    isTokenExpired() {
        var token = localStorage.getItem("token");
        if (token) {
            return this.helper.getTokenExpirationDate(token);
        }
    }

    updateSession(Id) {
        return this.genericService.post('api/auth/UpdateToken', Id);
    }

    getLocalStorageUser() {
        this.userData = JSON.parse(localStorage.getItem("user"));
        var token = localStorage.getItem("token");
        if (this.userData && token) {
            this.isLoggedIn = true;
            return true;
        } else {
            this.isLoggedIn = false;
            return false;
        }
    }
}
