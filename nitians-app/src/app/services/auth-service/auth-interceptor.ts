import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import {
    HttpRequest,
    HttpHandler,
    HttpEvent,
    HttpInterceptor,
    HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    login = false;
    constructor(private router: Router) { }
    // function which will be called for all http calls
    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(
            tap(
                event => {
                    // logging the http response to browser's console in case of a success
                    if (event instanceof HttpResponse) {
                        // console.log("api call success :", event);
                    }
                },
                error => {
                    // console.log("api call fail :", error);
                    if (request.url.indexOf('/auth/login') > -1) {
                        this.login = !this.login;
                    }
                    if (error && error.status === 401 && !this.login) {
                        localStorage.clear();
                        sessionStorage.clear();
                        this.router.navigate(['auth/login']);
                    } else if (event instanceof HttpResponse) {
                        console.log('api call error :', event);
                    }
                }
            )
        );
    }
}
