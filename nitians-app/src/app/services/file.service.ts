import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from '../../environments/environment';
import { Subject } from 'rxjs/internal/Subject';
import { tap } from 'rxjs/internal/operators/tap';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileService {
  SERVER_URL: string;
  updateList = new Subject<any>();
  setRefId = new Subject<any>();
  openModal = new Subject<any>();
  constructor(private httpClient: HttpClient) {
    this.SERVER_URL = environment.serverURL;
  }

  addFolder(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/folderMeta`, formRequest);
  }

  getFolderList(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/folderMeta/get`, formRequest);
  }

  getFolder(id): Observable<any> {
    return this.httpClient.get(`${this.SERVER_URL}/folderMeta/${id}`);
  }

  updateFolder(formRequest: any): Observable<any> {
    return this.httpClient.put(`${this.SERVER_URL}/folderMeta/${formRequest.id}`, formRequest);
  }

  addFile(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/fileMeta`, formRequest);
  }

  getFileList(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/fileMeta/get`, formRequest);
  }

  downloadAttachment(id: any) {
    return this.httpClient.get(`${this.SERVER_URL}/fileMeta/Download/${id}`, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }), responseType: 'blob'
    }).pipe(
      tap(
        // Log the result or error
        data => { },
        error => console.log(error)
      )
    );
  }
}
