import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class SiteService {
  SERVER_URL: string;
  constructor(private httpClient: HttpClient) {
    this.SERVER_URL = environment.serverURL;
  }
  index(model): Observable<any> {
    return this.httpClient.get(`${this.SERVER_URL}/site?model=${encodeURIComponent(JSON.stringify(model))}`);
  }
  get(id: any): Observable<any> {
    return this.httpClient.get(`${this.SERVER_URL}/site/${id}`);
  }
  post(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/site`, formRequest);
  }
  put(formRequest: any): Observable<any> {
    return this.httpClient.put(`${this.SERVER_URL}/site/${formRequest.Id}`, formRequest);
  }
  patch(formRequest: any): Observable<any> {
    return this.httpClient.patch(`${this.SERVER_URL}/site/${formRequest.Id}`, formRequest);
  }
  delete(id: any): Observable<any> {
    return this.httpClient.delete(`${this.SERVER_URL}/site/${id}`);
  }

}
