import { Injectable } from '@angular/core';
import { JwtHelperService } from "@auth0/angular-jwt";
const TOKEN_KEY = 'token';
const USER_KEY = 'user';
const USER_SITE = 'siteId'
@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {
  private redirectUrl: string = '';
  private loginUrl: string = 'auth';
  helper = new JwtHelperService();
  constructor() { }
  logOut() {
    localStorage.clear();
  }
  saveToken(token: string) {
    localStorage.removeItem(TOKEN_KEY);
    localStorage.setItem(TOKEN_KEY, token);
  }
  getToken(): string {
    return localStorage.getItem(TOKEN_KEY);
  }
  saveUser(user: any) {
    localStorage.removeItem(USER_KEY);
    localStorage.setItem(USER_KEY, JSON.stringify(user));
  }
  getUser() {
    return JSON.parse(localStorage.getItem(USER_KEY));
  }
  isUserLoggedIn(): Boolean {
    const userData = this.getUser();
    const token = this.getToken();
    if (userData && token) {
      return true;
    } else {
      return false;
    }
  }
  calculateTokenValidity() {
    const token = this.getToken();
    if (token) {
      var currentdate: any = new Date();
      var tokenValidity: any = this.isTokenExpired();
      var MS_PER_MINUTE = 60000;
      var myStartDate = new Date(tokenValidity);
      var CounterStartTime = myStartDate.getTime();
      var currentTime = currentdate.getTime();
      var counter = (CounterStartTime - currentTime) / MS_PER_MINUTE;
      return counter;
    } else {
      return 0;
    }
  }
  isTokenExpired() {
    const token = this.getToken();
    if (token) {
      return this.helper.getTokenExpirationDate(token);
    }
  }
  getRedirectUrl(): string {
    return this.redirectUrl;
  }
  setRedirectUrl(url: string): void {
    this.redirectUrl = url;
  }
  getLoginUrl(): string {
    return this.loginUrl;
  }
  getSiteId() {
    return 1;
  }
}