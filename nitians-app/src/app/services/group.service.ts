import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GenericService } from './generic/generic.service';
@Injectable({
  providedIn: 'root'
})
export class GroupService {
  SERVER_URL: string;
  constructor(private httpClient: HttpClient,private genericService:GenericService) {
    this.SERVER_URL = environment.serverURL;
  }
  index(model): Observable<any> {
    return this.httpClient.get(`${this.SERVER_URL}/group?model=${encodeURIComponent(JSON.stringify(model))}`);
  }
  get(id: any): Observable<any> {
    return this.httpClient.get(`${this.SERVER_URL}/group/${id}`);
  }
  post(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/group`, formRequest);
  }
  put(formRequest: any): Observable<any> {
    return this.httpClient.put(`${this.SERVER_URL}/group/${formRequest.Id}`, formRequest);
  }
  patch(formRequest: any): Observable<any> {
    return this.httpClient.patch(`${this.SERVER_URL}/group/${formRequest.Id}`, formRequest);
  }
  delete(id: any): Observable<any> {
    return this.httpClient.delete(`${this.SERVER_URL}/group/${id}`);
  }

  getSubjects(formRequest: any): Observable<any> {
    return this.httpClient.post(`${this.SERVER_URL}/groupSubject/getSubjectsOfGroup`, formRequest);
  }
}
