import { Injectable, Inject } from '@angular/core';
import {HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Http,Headers, RequestOptions, Response, ResponseContentType } from '@angular/http';
import { environment } from '../../../environments/environment';
import { AuthUtil } from 'src/app/utilities/auth-utilities';

@Injectable({
  providedIn: 'root'
})
export class GenericService {
    baseURL: string = environment.serverURL;
    constructor(private httpClient: HttpClient, private authUtil: AuthUtil, private http: Http) {
    }
    post(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders()
        };
        return this.httpClient.post(
            // "https://cors-anywhere.herokuapp.com/"+
            this.baseURL + url, data, httpOptions);
    }

    get(url: string) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Cache-Control': 'no-cache',
                Pragma: 'no-cache',
                'Content-Type': 'application/json',
                Authorization: this.authUtil.getTokenForHeader()

            })
        };
        return this.httpClient.get(this.baseURL + url, httpOptions);
    }

    put(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                Authorization: this.authUtil.getTokenForHeader()
            })
        };

        return this.httpClient.put(this.baseURL + url, data, httpOptions);
    }

    patch(url: string, data: any) {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                Authorization: this.authUtil.getTokenForHeader()
            })
        };
        return this.httpClient.patch(this.baseURL + url, data, httpOptions);
    }

    getDownloads(url: string) {
        const headers = new Headers({
            'Cache-Control': 'no-cache',
            Pragma: 'no-cache',
            'Content-Type': 'application/json',
            Authorization: this.authUtil.getTokenForHeader()
        });
        const options = new RequestOptions({ headers });
        options.responseType = ResponseContentType.Blob;

        return this.http.get(this.baseURL + url, options);
    }
}
