// import { GlobalUtil } from './global-utilities';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class AuthUtil {
  constructor(private storage: Storage, 
    // private globalUtil: GlobalUtil,
     private plt: Platform) { }
  login(resp: { token: any; user: { departments: any; }; permissions: any; }) {
    if (resp) {
      this.setToken(resp.token);
    //   this.setUserInfo(resp.user);
    //   this.setPermissions(resp.permissions);
    //   if (resp.user) {
    //     let depts = resp.user.departments;
    //     this.setAllDepartments(depts);
    //     if (depts && depts.length)
    //       this.globalUtil.setCurrentDepartment(depts[0]);
    //   }
    }
  }
  setToken(token: string) {
    localStorage.setItem('token', token);
  }
  getToken() {
    return localStorage.getItem('token');
  }
  setUserInfo(userObj: any) {
    localStorage.setItem('user', JSON.stringify(userObj));
  }
  getUserInfo(): any {
    return JSON.parse(localStorage.getItem('user') || null);
  }

  getTokenForHeader(): string {
    return 'Bearer ' + localStorage.getItem('token');
  }
  logout() {
    localStorage.clear();
  }
  isLoggedIn() {
    return this.getToken() ? true : false;
  }
  getPermissions() {
    return JSON.parse(localStorage.getItem('permissions') || null);
  }
  setPermissions(permissions) {
    var perms: any = null;
    if (permissions && permissions.length) {
      perms = {};
      for (let index = 0; index < permissions.length; index++) {
        let controller = permissions[index].controller;
        perms[controller] = permissions[index];
        if (perms[controller] && perms[controller].operations) {
          perms[controller].ops = {};
          for (let index2 = 0; index2 < perms[controller].operations.length; index2++) {
            perms[controller].ops[perms[controller].operations[index2]] = true;
          }

        }
      }
    }
    localStorage.setItem('permissions', JSON.stringify(perms));
  }

  setAllDepartments(departments: any) {
    localStorage.setItem('allDepartments', JSON.stringify(departments));
  }
}
