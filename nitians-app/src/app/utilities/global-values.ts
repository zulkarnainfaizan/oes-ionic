// import { ModalOptions } from 'ngx-bootstrap/modal/ngx-bootstrap-modal';
// import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
// import { formatDate } from '@angular/common';

// const config: ModalOptions = {
//   backdrop: 'static',
//   keyboard: false,
//   animated: true,
//   ignoreBackdropClick: true,
//   class: 'modal-lg',
//   //initialState: {
//   //  isModelWindowView: true, bodyStyle: 'row'
//   //}
// };
// const bsConfig: Partial<BsDatepickerConfig> = {
//   //dateInputFormat: 'YYYY-MM-DD',
//   containerClass: 'theme-dark-blue',
//   customTodayClass: 'custom-today-class',
//   isAnimated: true,
//   adaptivePosition: true
// };
// const locale = 'en-US';
// const format = 'yyyy-MM-dd';

// export class GlobalValues {
//   constructor() {
//   }

//   static getModalConfig() {
//     return config;
//   }

//   static getDatePickerConfig() {
//     return bsConfig;
//   }

//   static dateToLocale(date) {
//     if (date) {
//       var convertedDate = formatDate(date, format, locale);
//       return convertedDate;
//     }
//     else {
//       return;
//     }
//   }
// }
