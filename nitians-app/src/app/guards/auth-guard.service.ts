import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { TokenStorageService } from '../services/token-storage.service';
@Injectable()
export class AuthGuardService implements CanLoad, CanActivate {
    constructor(private router: Router, private tokenStorageService: TokenStorageService) {
    }
    canLoad(route: Route): boolean {
        let url: string = route.path;
        console.log(url);
        if (this.tokenStorageService.isUserLoggedIn()) {
            return true;
        }
        this.tokenStorageService.setRedirectUrl(url);
        this.router.navigate([this.tokenStorageService.getLoginUrl()]);
        return false;
    }
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
       
        let url: string = state.url;
        console.log(url,'canac');
        if (this.tokenStorageService.isUserLoggedIn()) {
            return true;
        }
        this.tokenStorageService.setRedirectUrl(url);
        this.router.navigate([this.tokenStorageService.getLoginUrl()]);
        return false;
    }
} 
